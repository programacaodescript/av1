#!/bin/bash

d=$(date +%Y-%m-%d)
w=$(date -d "$d" +%u)
x=$((3 - w))
[ $x -lt 0 ] && x=$((x + 7))
n=$(date -d "$d +$x days" +%Y-%m-%d)
nn=$(date -d "$n +7 days" +%Y-%m-%d)

echo "Próxima quarta-feira: $n"
echo "Quarta-feira seguinte: $nn"
