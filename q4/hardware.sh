#!/bin/bash

echo "Informações sobre o hardware do computador:"
echo "==========================================="

echo "Processador:"
echo "------------"
grep "model name" /proc/cpuinfo | head -n 1

echo ""
echo "Memória RAM:"
echo "------------"
grep "MemTotal" /proc/meminfo

echo ""
echo "Espaço em disco:"
echo "----------------"
df -h / | sed -n '2p'

echo ""
echo "Placa de vídeo:"
echo "---------------"
lspci | grep -i vga

echo ""
echo "Placa de rede:"
echo "--------------"
ip link | grep -E '^[0-9]+:' | sed 's/^[0-9]*: //'

echo ""
echo "Placa-mãe:"
echo "----------"
dmidecode -t baseboard | grep 'Product Name'

echo ""
echo "Sistema Operacional:"
echo "---------------------"
uname -a
