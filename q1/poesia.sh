#!/bin/bash

# Poesia com 5 frases
f="No meio do caminho tinha uma pedra,"
e="Tinha uma pedra no meio do caminho."
d="Tinha uma pedra"
c="No meio do caminho tinha uma pedra."
b="Nunca me esquecerei desse acontecimento"

# Imprimir cada frase com uma espera entre 0.5 e 3 segundos
echo -e "\033[0;$(($RANDOM % 7 + 31))m$f\033[0m"; sleep $(bc -l <<< "0.5 + ($RANDOM % 25) / 10")
echo -e "\033[0;$(($RANDOM % 7 + 31))m$e\033[0m"; sleep $(bc -l <<< "0.5 + ($RANDOM % 25) / 10")
echo -e "\033[0;$(($RANDOM % 7 + 31))m$d\033[0m"; sleep $(bc -l <<< "0.5 + ($RANDOM % 25) / 10")
echo -e "\033[0;$(($RANDOM % 7 + 31))m$c\033[0m"; sleep $(bc -l <<< "0.5 + ($RANDOM % 25) / 10")
echo -e "\033[0;$(($RANDOM % 7 + 31))m$b\033[0m"; sleep $(bc -l <<< "0.5 + ($RANDOM % 25) / 10")
