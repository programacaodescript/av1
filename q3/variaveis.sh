#!/bin/bash

# Criando variáveis no bash
a="Valor direto"

echo "Digite um valor para a variável b:"
read b

c=$1

echo "Valor da variável a: $a"
echo "Valor da variável b (digitada pelo usuário): $b"
echo "Valor da variável c (recebida como parâmetro de linha de comando): $c"

# Explicação:
echo "A diferença entre pedir explicitamente para o usuário digitar um valor e receber como parâmetro de linha de comando é que, no primeiro caso, o script pausa a execução e espera que o usuário forneça o valor, enquanto no segundo caso o valor é passado como argumento durante a execução do script."

echo "Nome do script: $0"
echo "Número total de argumentos passados: $#"
echo "Todos os argumentos passados: $@"
echo "Último argumento passado: ${!#}"
echo "Código de retorno do último comando: $?"


